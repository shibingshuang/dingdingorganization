package com.ding.dev.service;


import com.alibaba.fastjson.JSON;
import com.ding.dev.component.DingDingConstants;
import com.ding.dev.domain.Person;
import com.ding.dev.domain.Unit;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.*;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.stereotype.Service;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import java.util.*;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

/**
 *
 */
@Service
public class LdapServices {
    @Autowired
    private LdapTemplate ldapTemplate;

    /*对用户的处理*/
    public Person findByCn(String cn){
        return ldapTemplate.findOne(query().where("cn").is(cn),Person.class);
    }
    public Person findPersonByDn(Name dn){
        return ldapTemplate.findByDn(dn,Person.class);
    }

    public Object lookupPerson(Name dn){
        return ldapTemplate.lookup(dn);
    }

    public List<Person> findAllPerson(){
        return  ldapTemplate.findAll(Person.class);
    }

    public void createPerson(Person person){
        ldapTemplate.create(person);
    }

    public void updatePerson(Person person){
        ldapTemplate.update(person);
    }

    public void deletePerson(Person person){
        ldapTemplate.delete(person);
    }

    public List<String> getAllUidNumber(String uidNumber){
        return ldapTemplate.search(query().where("uidNumber").is(uidNumber),new OneAttributesMapper());
    }

    public List<Person> getPersonByCn(String cn){
        return ldapTemplate.search(query().where("cn").is(cn),new CnAttributesMapper());
    }

    public List<Person> getPersonByAttr(String attr,String value){
        return ldapTemplate.search(query().where(attr).is(value),new PersonAttributesMapper());
    }


    /*对组织的处理*/
    public List<Unit> findAllUnit(){

        return ldapTemplate.search(query()
                .where("objectclass").is("organizationalUnit"), new UnitAttributesMapper());
    }

    public List<Unit> getUnitByAttr(String attr,String value){
        return  ldapTemplate.search(query()
                .where(attr).is(value), new UnitAttributesMapper());
    }

    public Unit findUnitByDn(Name dn){
        return ldapTemplate.findByDn(dn,Unit.class);
    }

    public void createUnit(Unit unit){
        ldapTemplate.create(unit);
    }

    public void updateUnit(Unit unit){
        ldapTemplate.update(unit);
    }

    public void deleteUnit(Unit unit){
        ldapTemplate.delete(unit);
    }

    public void rename(Name oldDn,Name newDn){
        ldapTemplate.rename(oldDn,newDn);

    }

    /*添加用户到组*/
    public void addMemberToGroup(Name groupDn, Person person) {
        //Name userDn = person.getDn();
        DirContextOperations ctx = ldapTemplate.lookupContext(groupDn);
        String[] memberuidArr = ctx.getStringAttributes("memberuid");
        List<String> list = Lists.newArrayList(Arrays.asList(memberuidArr));
        list.add(person.getUid());
        Object[] arr = list.toArray();
        ctx.setAttributeValues("memberuid",arr);
        ldapTemplate.modifyAttributes(ctx);
    }

    public List<Name> getDn(String base_dn, String fiStr) {
        List<Name> eList = ldapTemplate.search(base_dn, fiStr, SearchControls.SUBTREE_SCOPE,
                new ContextMapper() {
                    public Name mapFromContext(Object ctx) {
                        DirContextAdapter context = (DirContextAdapter)ctx;
                        Name dn = context.getDn();
                        return dn;
                    }
                });
        return eList;
    }


    private class UnitAttributesMapper implements AttributesMapper<Unit> {
        public Unit mapFromAttributes(Attributes attrs) throws NamingException {
            Unit unit = new Unit();
            String ou = (String)attrs.get("ou").get();
            String destinationIndicator = (String) attrs.get("destinationIndicator").get();
            Name dn = null;
            List<Name> list = getDn("","(destinationIndicator="+destinationIndicator+")");
            if(list != null && list.size()>0){
                dn = list.get(0);
            }
            /*String arr[] = description.split(",");
            LdapNameBuilder builder = LdapNameBuilder.newInstance();
            for(int i=arr.length-1;i>=0;i--){
                String str[] = arr[i].split("=");
                builder = builder.add(str[0],str[1]);
            }
            Name dn = builder.build();*/
            unit.setDn(dn);
            unit.setOu(ou);
            unit.setDestinationIndicator(destinationIndicator);
            return unit;
        }
    }

    private class PersonAttributesMapper implements AttributesMapper<Person> {
        public Person mapFromAttributes(Attributes attrs) throws NamingException {
            Person person = new Person();
            String uid = (String)attrs.get("uid").get();
            person.setUid(uid);
            String destinationIndicator = (String) attrs.get("destinationIndicator").get();
            person.setShadowExpire((String) attrs.get("shadowExpire").get());
            person.setShadowLastChange((String) attrs.get("shadowExpire").get());
            person.setShadowInactive((String) attrs.get("shadowInactive").get());
            person.setShadowWarning((String) attrs.get("shadowWarning").get());
            person.setShadowMax((String) attrs.get("shadowMax").get());
            person.setShadowFlag((String) attrs.get("shadowFlag").get());
            person.setUserPassword(new String(JSON.toJSONString(attrs.get("userPassword").get()).getBytes()));
            person.setUidNumber((String) attrs.get("uidNumber").get());
            person.setGidNumber((String) attrs.get("gidNumber").get());
            person.setGivenName((String) attrs.get("givenName").get());
            String ou = (String) attrs.get("ou").get();
            person.setOu(ou);
            person.setDestinationIndicator(destinationIndicator);
            List<Name> list = getDn("","(destinationIndicator="+destinationIndicator+")");
            Name dn = null;
            if(list != null && list.size()>0){
                dn = list.get(0);
            }
            /*String arr[] = description.split(",");
            LdapNameBuilder builder = LdapNameBuilder.newInstance();
            for(int i=arr.length-1;i>=0;i--){
                String str[] = arr[i].split("=");
                builder = builder.add(str[0],str[1]);
            }
            Name dn = builder.build();*/
            person.setDn(dn);
            person.setHomeDirectory((String) attrs.get("homeDirectory").get());
            person.setMobile((String) attrs.get("mobile").get());
            person.setMail((String) attrs.get("mail").get());
            person.setSn((String) attrs.get("sn").get());
            person.setCn((String) attrs.get("cn").get());
            person.setDisplayName((String) attrs.get("displayName").get());
            person.setShadowMin((String) attrs.get("shadowMin").get());

            return person;
        }
    }

    private class OneAttributesMapper implements AttributesMapper<String> {
        public String mapFromAttributes(Attributes attrs) throws NamingException {
            String str = (String)attrs.get("uidNumber").get();
            return str;
        }
    }

    private class CnAttributesMapper implements AttributesMapper<Person> {
        public Person mapFromAttributes(Attributes attrs) throws NamingException {
            Person person = new Person();
            person.setCn((String)attrs.get("cn").get());
            person.setDestinationIndicator((String) attrs.get("destinationIndicator").get());
            return  person;

        }
    }


}
