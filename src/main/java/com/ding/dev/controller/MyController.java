package com.ding.dev.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Shibs on 2020/6/25.
 */

@Controller
public class MyController {
    @RequestMapping("/index")
    public String test(){
        return "index";
    }

}
