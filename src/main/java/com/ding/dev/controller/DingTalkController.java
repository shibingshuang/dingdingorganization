package com.ding.dev.controller;

import com.alibaba.fastjson.JSONObject;
import com.ding.dev.aes.DingTalkEncryptException;
import com.ding.dev.aes.DingTalkEncryptor;
import com.ding.dev.component.*;
import com.ding.dev.domain.Person;
import com.ding.dev.domain.Unit;
import com.ding.dev.service.LdapServices;
import com.dingtalk.api.response.OapiDepartmentGetResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.google.common.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.web.bind.annotation.*;

import javax.naming.Name;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Shibs on 2020/6/23.
 */
@RestController
public class DingTalkController {
    Logger logger = LoggerFactory.getLogger(DingDingAccessTokenTask.class);

    @Autowired
    private LdapServices ldapServices;
    @Autowired
    private Cache<String, Integer> cache;


    /**
     * 获取变更用户及部门
     */
    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/dingtalk/callbackreceive", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String addCallbackreceive(HttpServletRequest request, HttpServletResponse response) {
        String result = "false";
        JSONObject decodeEncryptJson = null;
        try {
            /** url中的签名 **/
            String msgSignature = request.getParameter("signature");
            /** url中的时间戳 **/
            String timeStamp = request.getParameter("timestamp");
            /** url中的随机字符串 **/
            String nonce = request.getParameter("nonce");
            /** 取得JSON对象中的encrypt字段 **/
            String encrypt = "";

            ServletInputStream sis = request.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(sis));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            JSONObject jsonEncrypt = JSONObject.parseObject(sb.toString());
            String res = "success"; //res是需要返回给钉钉服务器的字符串，一般为success;
            if (jsonEncrypt != null && jsonEncrypt.size() > 0 && jsonEncrypt.containsKey("encrypt")) {
                encrypt = jsonEncrypt.getString("encrypt");

                String decodeEncrypt = new DingTalkEncryptor(DingDingConstants.TOKEN,DingDingConstants.AES_KEY,DingDingConstants.CorpId).getDecryptMsg(msgSignature, timeStamp, nonce, encrypt); //密文解密
                decodeEncryptJson = JSONObject.parseObject(decodeEncrypt);
                logger.info("请求参数"+decodeEncryptJson.toJSONString());

                String eventType = decodeEncryptJson.getString("EventType"); //回调类型

                if (cache.getIfPresent(eventType) == null) {// 如果缓存中有这个事件视为重复
                    logger.info("判断缓存-------NO");
                    cache.put(eventType, 0);
                    analysisType(decodeEncryptJson);
                } else {
                    logger.error("消息重复");
                }
                result = codeEncrypt(res, timeStamp, nonce).toString();
                logger.info("返回了");
            }

        } catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 对返回信息进行加密
     * @param res
     * @param timeStamp
     * @param nonce
     * @return
     */
    public static JSONObject codeEncrypt(String res, String timeStamp, String nonce){
        long timeStampLong = Long.parseLong(timeStamp);
        Map<String, String> jsonMap = null;
        try {
            jsonMap = new DingTalkEncryptor(DingDingConstants.TOKEN,DingDingConstants.AES_KEY,DingDingConstants.CorpId).getEncryptedMap(res, timeStampLong, nonce); //jsonMap是需要返回给钉钉服务器的加密数据包
        } catch (DingTalkEncryptException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        JSONObject json = new JSONObject();
        json.putAll(jsonMap);
        return json;
    }


    //分析回调类型
    public void analysisType(JSONObject decodeEncryptJson) throws NoSuchAlgorithmException {
        String eventType = "";
        String[] userIdArr = {};
        String[] deptIdArr = {};
        Person person = new Person();
        Unit unit = new Unit();
        DingDingCommenUtil com = new DingDingCommenUtil();
        LdapUtil ldapUtil = new LdapUtil();
        if (decodeEncryptJson != null && decodeEncryptJson.size() > 0 && decodeEncryptJson.containsKey("EventType")) {
            eventType = decodeEncryptJson.getString("EventType"); //回调类型
            logger.info("回调类型为"+eventType);

            if(decodeEncryptJson.containsKey("UserId")){
                //变更用户id
                userIdArr = decodeEncryptJson.getJSONArray("UserId").toString().split(",");
                for(String userId:userIdArr){
                    userId = userId.substring(2,userId.length()-2);
                    person.setDestinationIndicator(userId);
                    if(DingDingConstants.USER_ADD_ORG.equals(eventType)){
                        person.setUidNumber(getRandom());
                    }else{
                        List<Person> list = ldapServices.getPersonByAttr("destinationIndicator",userId);
                        if(list != null && list.size()>0){
                            person = list.get(0);
                        }
                    }
                    if(!DingDingConstants.USER_LEAVE_ORG.equals(eventType)){
                        //根据userId获取用户信息
                        OapiUserGetResponse userResponse = com.getUserInfo(userId);
                        //姓名
                        String name = userResponse.getName();

                        //企业邮箱
                        String orgEmail = userResponse.getOrgEmail();

                        //获取拼音
                        String name_pinyin = getPinYinAll(name,userId);
                        if(orgEmail != null){
                            name_pinyin = orgEmail.substring(0,orgEmail.indexOf("@"));
                        }else{
                            orgEmail = name_pinyin+"@sunyur.com";
                        }

                        //手机号
                        String moblie = userResponse.getMobile();
                        //获取uidNumber

                        //所在部门id
                        String deptStr = userResponse.getOrderInDepts();
                        String deptId = deptStr.substring(1,deptStr.indexOf(":"));

                        //获取部门信息
                        OapiDepartmentGetResponse deptResponse = com.getDeptInfo(deptId);
                        logger.info("所在部门信息："+deptResponse.getBody());
                        //部门名称
                        String deptName = deptResponse.getName();
                        person.setOu(deptName);
                        //父部门
                        String deptParentId = deptResponse.getParentid().toString();
                        String deptList = deptName;
                        while(!"1".equals(deptParentId)){
                            //获取部门信息
                            OapiDepartmentGetResponse deptRes = com.getDeptInfo(deptParentId);
                            deptParentId = deptRes.getParentid().toString();
                                    //部门名称
                            deptName = deptRes.getName();
                            deptList += ","+deptName;
                        }
                        deptList += ","+DingDingConstants.ParentDept;

                        String deptArr[] = deptList.split(",");
                        LdapNameBuilder builder = LdapNameBuilder.newInstance();
                        for(int i=deptArr.length-1;i>=0;i--){
                            builder = builder.add("ou",deptArr[i]);
                        }

                        Name dn = builder.add("uid",name_pinyin).build();
                        System.out.println(dn);
                        person.setDn(dn);
                        person.setCn(name_pinyin);
                        person.setSn(name);
                        person.setGivenName(name_pinyin);
                        person.setUid(name_pinyin);
                        person.setMail(orgEmail);
                        person.setMobile(moblie);
                        person.setDisplayName(name_pinyin);
                        //初始化密码
                        String userPassword = ldapUtil.generateSSHA(name_pinyin.getBytes());
                        person.setUserPassword(userPassword);
                        person.setHomeDirectory(name_pinyin);
                        person.setGidNumber(DingDingConstants.GidNumber);
                        person.setO(DingDingConstants.O);
                        person.setShadowFlag("0");
                        person.setShadowMin("0");
                        person.setShadowMax("99999");
                        person.setShadowWarning("0");
                        person.setShadowInactive("99999");
                        person.setShadowLastChange("12011");
                        person.setShadowExpire("99999");
                    }
                }
            }else if(decodeEncryptJson.containsKey("DeptId")){
                //变更部门id
                String deptIdStr = decodeEncryptJson.getJSONArray("DeptId").toString();
                deptIdArr = deptIdStr.substring(1,deptIdStr.length()-1).split(",");
                for(String deptId:deptIdArr){
                    unit.setDestinationIndicator(deptId);
                    if(!DingDingConstants.ORG_DEPT_CREATE.equals(eventType)){
                        List<Unit> list = ldapServices.getUnitByAttr("destinationIndicator",deptId);
                        if(list != null && list.size()>0){
                            unit = list.get(0);
                        }
                    }

                    if(!DingDingConstants.ORG_DEPT_REMOVE.equals(eventType)){
                        //根据部门id获取部门信息
                        OapiDepartmentGetResponse deptResponse = com.getDeptInfo(deptId);
                        logger.info("变更的部门信息："+deptResponse.getBody());
                        //获取部门名称
                        String deptName = deptResponse.getName();
                        //获取父部门
                        String deptParentId = deptResponse.getParentid().toString();
                        String deptList = deptName;
                        while(!"1".equals(deptParentId)){
                            //获取部门信息
                            OapiDepartmentGetResponse deptRes = com.getDeptInfo(deptParentId);
                            deptParentId = deptRes.getParentid().toString();
                                    //部门名称
                            String deptN = deptRes.getName();
                            deptList += ","+deptN;
                        }
                        deptList += ","+DingDingConstants.ParentDept;

                        String deptArr[] = deptList.split(",");
                        LdapNameBuilder builder = LdapNameBuilder.newInstance();
                        for(int i=deptArr.length-1;i>=0;i--){
                            builder = builder.add("ou",deptArr[i]);
                        }
                        Name dn = builder.build();
                        unit.setDn(dn);
                        unit.setOu(deptName);
                    }

                }
            }
        }
        //根据不同的回调类型进行不同处理
        handleEvent(eventType,person,unit);

    }


    //根据不同的回调类型进行不同处理
    public void handleEvent(String eventType,Person person,Unit unit){
        switch(eventType){
            case DingDingConstants.USER_ADD_ORG:
                logger.info("通讯录用户增加");
                ldapServices.createPerson(person);
                ldapServices.addMemberToGroup(DingDingConstants.groupDn,person);
                break;
            case DingDingConstants.USER_MODIFY_ORG:
                logger.info("通讯录用户更改");
                //判断是否修改了uid和部门
                List<Person> list1 = ldapServices.getPersonByAttr("destinationIndicator",person.getDestinationIndicator());
                if(list1 != null && list1.size()>0){
                    Person personOld = list1.get(0);
                    if(person.getUid().equals(personOld.getUid()) && person.getOu().equals(personOld.getOu())){
                        person.setUidNumber(personOld.getUidNumber());
                        ldapServices.updatePerson(person);
                    }else{
                        ldapServices.rename(personOld.getDn(),person.getDn());
                        person.setUidNumber(getRandom());
                        ldapServices.updatePerson(person);
                        //ldapServices.deletePerson(personOld);
                        //ldapServices.createPerson(person);
                    }
                }else{
                    logger.info("Ldap中用户不存在："+person.toString());
                }

                break;
            case DingDingConstants.USER_LEAVE_ORG:
                logger.info("通讯录用户离职");
                ldapServices.deletePerson(person);
                break;
            case DingDingConstants.USER_ACTIVE_ORG:
                logger.info("加入企业后用户激活");
                break;
            case DingDingConstants.ORG_ADMIN_ADD:
                logger.info("通讯录用户被设为管理员");
                break;
            case DingDingConstants.ORG_ADMIN_REMOVE:
                logger.info("通讯录用户被取消设置管理员");
                break;
            case DingDingConstants.ORG_DEPT_CREATE:
                logger.info("通讯录企业部门创建");
                ldapServices.createUnit(unit);
                break;
            case DingDingConstants.ORG_DEPT_MODIFY:
                logger.info("通讯录企业部门修改");

                List<Unit> list2 = ldapServices.getUnitByAttr("destinationIndicator",unit.getDestinationIndicator());
                if(list2 != null && list2.size()>0){
                    Unit unitOld = list2.get(0);
                    //判断是否修改了部门名称、上级部门
                    if(unit.getOu().equals(unitOld.getOu()) && unit.getDn().toString().equals(unitOld.getDn().toString())){
                        ldapServices.updateUnit(unit);
                    }else{
                        ldapServices.rename(unitOld.getDn(),unit.getDn());
                        //ldapServices.deleteUnit(unitOld);
                        //ldapServices.createUnit(unit);
                    }
                }else{
                    logger.info("Ldap中部门不存在："+unit.toString());
                }

                break;
            case DingDingConstants.ORG_DEPT_REMOVE:
                logger.info("通讯录企业部门删除");
                ldapServices.deleteUnit(unit);
                break;
            case DingDingConstants.ORG_REMOVE:
                logger.info("企业被解散");
                break;
            case DingDingConstants.ORG_CHANGE:
                logger.info("企业信息发生变更");
                break;
            case DingDingConstants.LABEL_USER_CHANGE:
                logger.info("员工角色信息发生变更");
                break;

            case DingDingConstants.LABEL_CONF_ADD:
                logger.info("增加角色或者角色组");
                break;
            case DingDingConstants.LABEL_CONF_DEL:
                logger.info("删除角色或者角色组");
                break;
            case DingDingConstants.LABEL_CONF_MODIFY:
                logger.info("修改角色或者角色组");
                break;

        }
    }



    //随机获取uidNumber
    public String getRandom(){
        int num = (int)((Math.random()*9+1)*10000);
        //判断是否重复
        List<String> list = ldapServices.getAllUidNumber(String.valueOf(num));
        if(!list.isEmpty()){
            getRandom();
        }
        return String.valueOf(num);
    }


    public String getPinYinAll(String str,String userId) {
        Pinyin pinyin = new Pinyin();
        String result = "";
        result = pinyin.getPinYin(str)+"."+pinyin.getPinYinHeadChar(str);
        //判断是否重名
        List<Person> list = ldapServices.getPersonByCn(result);
        if(list != null && list.size() > 0){
            Person person = list.get(0);
            if(!userId.equals(person.getDestinationIndicator())){
                String regEx="[^0-9]";
                Pattern p = Pattern.compile(regEx);
                Matcher m = p.matcher(result);
                String digit = m.replaceAll("").trim();
                if("".equals(digit)){
                    result = pinyin.getPinYin(str)+1+"."+pinyin.getPinYinHeadChar(str);
                }else{
                    int n = Integer.parseInt(digit)+1;
                    result = result.replaceAll(digit,String.valueOf(n));
                }
            }else{
                return result;
            }

        }
        return result;
    }




}
