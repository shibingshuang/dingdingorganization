package com.ding.dev.controller;

import com.ding.dev.component.DingDingAccessTokenTask;
import com.ding.dev.component.DingDingCommenUtil;
import com.ding.dev.component.DingDingConstants;
import com.ding.dev.domain.ResponseInfo;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiUserGetuserinfoRequest;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.dingtalk.api.response.OapiUserGetuserinfoResponse;
import com.taobao.api.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Shibs on 2020/6/25.
 */
@Controller
public class GetUserInfo {

    Logger logger = LoggerFactory.getLogger(DingDingAccessTokenTask.class);

    @GetMapping("/dingtalk/getUserInfo")
    @ResponseBody
    public ResponseInfo getUserInfo(String code) {
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.GET_USER_ID);
        OapiUserGetuserinfoRequest request = new OapiUserGetuserinfoRequest();
        request.setCode(code);
        request.setHttpMethod("GET");
        OapiUserGetuserinfoResponse response = null;
        ResponseInfo resDomain = new ResponseInfo();
        try {
            response = client.execute(request, DingDingAccessTokenTask.token);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        logger.info("用户id为："+response.getUserid());
        if("ok".equals(response.getErrmsg())){
            resDomain.setSuccess(true);
            DingDingCommenUtil com = new DingDingCommenUtil();
            OapiUserGetResponse res = com.getUserInfo(response.getUserid());
            resDomain.setData(res.getName());
        }
        return resDomain;
    }
}
