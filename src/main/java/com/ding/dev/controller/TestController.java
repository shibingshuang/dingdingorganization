package com.ding.dev.controller;

import com.ding.dev.component.DingDingCommenUtil;
import com.ding.dev.component.RegisterEvent;
import com.ding.dev.domain.Person;
import com.ding.dev.domain.Unit;
import com.ding.dev.service.LdapServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.Name;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * create by shibs on 2020/7/31 0031
 */
@RestController
public class TestController {

    @Autowired
    private LdapServices ldapServices;

    @Autowired
    private RegisterEvent registerEvent;

    //验证服务器的有效性
    @RequestMapping(value="/dingding",method = RequestMethod.GET)
    public void check(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("hello");
    }

    //注册回调url
    @RequestMapping(value="/dingding/register",method = RequestMethod.POST)
    public Object register(@RequestParam("register_url") String url) {
        String msg = registerEvent.registerMain(url);
        if("ok".equals(msg)){
            return "提交成功！";
        }else{
            return "提交失败------>"+msg;
        }

    }


    @RequestMapping(value = "/findOnePersion",method = RequestMethod.GET)
    public Person findAllPersion(){
        return ldapServices.findByCn("shibingshuang.sbs");

    }


    @RequestMapping(value = "/findPersion",method = RequestMethod.GET)
    public String findPersion(){
        String r = "";
        Name dn = LdapNameBuilder.newInstance()
                .add("ou","父一")
                .add("uid", "gongjunlai.gjl2")
                .build();
        Object o = ldapServices.lookupPerson(dn);
        //DirContextAdapter context = (DirContextAdapter)o;
        if(o != null){
            System.out.println(o.toString());
            r = o.toString();
        }
        return r;

    }


    @RequestMapping(value = "/createPerson",method = RequestMethod.GET)
    public void createPerson(){
        Person person = new Person();
        Name dn = LdapNameBuilder.newInstance()
                .add("ou","测新建部门6")
                .add("uid", "yangmingzhu.ymz")
                .build();
        System.out.println(dn);
        person.setDn(dn);
        person.setOu("测新建部门6");
        person.setCn("yangmingzhu.ymz");
        person.setGidNumber("0");
        person.setSn("凯林");
        person.setUidNumber("11111");
        person.setUid("yangmingzhu.ymz");
        person.setMail("yangmingzhu.ymz@sunyur.com");
        person.setMobile("17823425566");
        person.setHomeDirectory("/yangmingzhu.ymz");
        ldapServices.createPerson(person);
    }

    @RequestMapping(value = "/deletePerson",method = RequestMethod.GET)
    public void deletePerson(){
        Person person = new Person();
        Name dn = LdapNameBuilder.newInstance()
                .add("uid", "gaokailin.gkl")
                .build();
        person.setDn(dn);
        ldapServices.deletePerson(person);

    }

    @RequestMapping(value = "/updatePerson",method = RequestMethod.GET)
    public void updatePerson(){
        Person person = new Person();
        Name dn = LdapNameBuilder.newInstance()
                .add("ou","越天八部")
                .add("ou","测试")
                .add("uid", "gaokailin.gkl")
                .build();
        person.setDn(dn);
        person.setGivenName("高凯林");
        person.setOu("越天八部");
        person.setCn("gaokailin.gkl");
        person.setGidNumber("0");
        person.setSn("凯林");
        person.setUidNumber("11121");
        person.setUid("gaokailin.gkl");
        person.setMail("gaokailin.gkl@sunyur.com");
        person.setMobile("17823425566");
        person.setHomeDirectory("/gkl");
        ldapServices.updatePerson(person);

    }

    @RequestMapping(value = "/findAllPerson",method = RequestMethod.GET)
    public List<Person> findAllPerson(){
        return ldapServices.findAllPerson();
    }

    @RequestMapping(value = "/createUnit",method = RequestMethod.GET)
    public void createUnit(){
        Unit unit = new Unit();
        Name dn = LdapNameBuilder.newInstance()
                .add("ou","越天一部")
                .build();
        unit.setDn(dn);
        unit.setOu("一部");
        unit.setDescription("一部");
        ldapServices.createUnit(unit);
    }

    @RequestMapping(value = "/updateUnit",method = RequestMethod.GET)
    public void updateUnit(){
        Unit unit = new Unit();
        Name dn = LdapNameBuilder.newInstance()
                .add("ou","越天一部")
                .add("ou","一部")
                .build();
        unit.setDn(dn);
        unit.setOu("一部");
        unit.setDescription("一部呀");
        ldapServices.updateUnit(unit);
    }

    @RequestMapping(value = "/deleteUnit",method = RequestMethod.GET)
    public void deleteUnit(){
        Unit unit = new Unit();
        Name dn = LdapNameBuilder.newInstance()
                .add("ou","越天一部")
                .build();
        unit.setDn(dn);
        unit.setOu("一部");
        unit.setDescription("一部");
        ldapServices.deleteUnit(unit);
    }

    @RequestMapping(value = "/findAllUnit",method = RequestMethod.GET)
    public List<Unit> findAllUnit(){
        return ldapServices.findAllUnit();
    }

    @RequestMapping(value = "/findOneUnit",method = RequestMethod.GET)
    public List<Unit> findOneUnit(){
        return ldapServices.getUnitByAttr("destinationIndicator","380164285");
    }

    @RequestMapping(value = "/rename",method = RequestMethod.GET)
    public void rename(){
        Name oldDn = LdapNameBuilder.newInstance()
                .add("ou","父部门")
                .add("ou","子部门二")
                .build();
        Name newDn = LdapNameBuilder.newInstance()
                .add("ou","父部门")
                .add("ou","子部门三")
                .build();

        ldapServices.rename(oldDn,newDn);
    }

    @RequestMapping(value = "/getDn",method = RequestMethod.GET)
    public List<String> getDn(){
        List<Name> list = ldapServices.getDn("uid=gongjunlai.gjl2,ou=父一","(objectclass=inetOrgPerson)");
        List<String> resultList = new ArrayList<>();
        if(list != null && list.size()>0){
            for(Name dn:list){
                resultList.add(dn.toString());
            }

        }

        return resultList;
    }

}