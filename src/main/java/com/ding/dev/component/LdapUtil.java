package com.ding.dev.component;

import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

/*
 * create by shibs on 2020/7/3 0003
 */
@Component
public class LdapUtil {

    private static final int SALT_LENGTH = 4;

    /**
     *  初始化LdapTemplate
     * @return
     */
    public static LdapTemplate getLdapTemplate(){
        LdapTemplate template = null;
        try {
            LdapContextSource contextSource = new LdapContextSource();

            String url = "ldap://172.26.108.146:389";
            String base = "dc=sunyur,dc=com";
            String userDn = "cn=admin,dc=sunyur,dc=com";
            String password = "sunyurldap123";

            contextSource.setUrl(url);
            contextSource.setBase(base);
            contextSource.setUserDn(userDn);
            contextSource.setPassword(password);
            contextSource.setPooled(false);
            contextSource.afterPropertiesSet(); // important
            template = new LdapTemplate(contextSource);
        }catch (Exception e){
            e.printStackTrace();
        }
        return template;
    }

    public static String generateSSHA(byte[] password)
            throws NoSuchAlgorithmException {
        SecureRandom secureRandom = new SecureRandom();
        byte[] salt = new byte[SALT_LENGTH];
        secureRandom.nextBytes(salt);
        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
        crypt.reset();
        crypt.update(password);
        crypt.update(salt);
        byte[] hash = crypt.digest();
        byte[] hashPlusSalt = new byte[hash.length + salt.length];
        System.arraycopy(hash, 0, hashPlusSalt, 0, hash.length);
        System.arraycopy(salt, 0, hashPlusSalt, hash.length, salt.length);
        return new StringBuilder().append("{SSHA}")
                .append(Base64.getEncoder().encodeToString(hashPlusSalt))
                .toString();
    }


}