package com.ding.dev.component;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiCallBackDeleteCallBackRequest;
import com.dingtalk.api.request.OapiCallBackGetCallBackRequest;
import com.dingtalk.api.request.OapiCallBackRegisterCallBackRequest;
import com.dingtalk.api.request.OapiCallBackUpdateCallBackRequest;
import com.dingtalk.api.response.OapiCallBackDeleteCallBackResponse;
import com.dingtalk.api.response.OapiCallBackGetCallBackResponse;
import com.dingtalk.api.response.OapiCallBackRegisterCallBackResponse;
import com.dingtalk.api.response.OapiCallBackUpdateCallBackResponse;
import com.taobao.api.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shibs on 2020/6/15.
 * 注册回调接口
 */
@Component
public class RegisterEvent {
    @Autowired
    private DingDingCommenUtil dingdingCommenUtil;

    public String registerMain(String url) {
        String errmsg = "";
        String accesstoken = dingdingCommenUtil.getToken(DingDingConstants.APPKEY, DingDingConstants.APPSECRET).getAccess_token();
        String token = DingDingConstants.TOKEN;
        String aesKey = DingDingConstants.AES_KEY;
        String callBackUrl = url;

        List<String> listStr = new ArrayList<String>();
        listStr.add(DingDingConstants.USER_ADD_ORG);
        listStr.add(DingDingConstants.USER_MODIFY_ORG);
        listStr.add(DingDingConstants.USER_LEAVE_ORG);
        listStr.add(DingDingConstants.USER_ACTIVE_ORG);
        listStr.add(DingDingConstants.ORG_ADMIN_ADD);
        listStr.add(DingDingConstants.ORG_ADMIN_REMOVE);
        listStr.add(DingDingConstants.ORG_DEPT_CREATE);
        listStr.add(DingDingConstants.ORG_DEPT_MODIFY);
        listStr.add(DingDingConstants.ORG_DEPT_REMOVE);
        listStr.add(DingDingConstants.ORG_REMOVE);
        listStr.add(DingDingConstants.ORG_CHANGE);
        listStr.add(DingDingConstants.LABEL_USER_CHANGE);
        listStr.add(DingDingConstants.LABEL_CONF_ADD);
        listStr.add(DingDingConstants.LABEL_CONF_DEL);
        listStr.add(DingDingConstants.LABEL_CONF_MODIFY);
        try {
            //errmsg = deleteRegisterCallBack(accesstoken);
            errmsg = selectRegisterCallBack(accesstoken);
            System.out.println("查询注册事件返回：" + errmsg);
            if(errmsg != null){
                errmsg = updateRegisterCallBack(accesstoken, listStr, token, aesKey, callBackUrl);
                System.out.println("更新注册事件返回：" + errmsg);
            }else{
                errmsg = registerCallBack(accesstoken, listStr, token, aesKey, callBackUrl);
                System.out.println("添加注册事件返回：" + errmsg);
            }

        } catch (ApiException e) {
            e.printStackTrace();
        }
        return errmsg;

    }


    /**
     * 注册事件回调接口
     * @param accesstoken 企业的accesstoken
     * @param callBackTag 需要监听的事件类型，共有20种（Array[String]）
     * @param token 加解密需要用到的token，ISV(服务提供商)推荐使用注册套件时填写的token，普通企业可以随机填写
     * @param aesKey 数据加密密钥。用于回调数据的加密，长度固定为43个字符，从a-z, A-Z, 0-9共62个字符中选取,您可以随机生成，ISV(服务提供商)推荐使用注册套件时填写的EncodingAESKey
     * @param callBackUrl 接收事件回调的url
     * @return
     * @throws ApiException
     */
    public static String registerCallBack(String accesstoken, List<String> callBackTag, String token, String aesKey, String callBackUrl) throws ApiException {
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.REGISTER_CALL_BACK.replace("ACCESS_TOKEN",accesstoken));
        OapiCallBackRegisterCallBackRequest request = new OapiCallBackRegisterCallBackRequest();
        request.setUrl(callBackUrl);
        request.setAesKey(aesKey);
        request.setToken(token);
        request.setCallBackTag(callBackTag);
        OapiCallBackRegisterCallBackResponse response = client.execute(request,accesstoken);
        return response.getErrmsg();
    }

    public  static String updateRegisterCallBack(String accesstoken, List<String> callBackTag, String token, String aesKey, String callBackUrl) throws ApiException {
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.UPDATE_CALL_BACK.replace("ACCESS_TOKEN",accesstoken));
        OapiCallBackUpdateCallBackRequest request = new OapiCallBackUpdateCallBackRequest ();
        request.setUrl(callBackUrl);
        request.setAesKey(aesKey);
        request.setToken(token);
        request.setCallBackTag(callBackTag);
        OapiCallBackUpdateCallBackResponse  response = response = client.execute(request,accesstoken);
        return response.getErrmsg();
    }

    public  static String deleteRegisterCallBack(String accesstoken) throws ApiException {
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.DELETE_CALL_BACK.replace("ACCESS_TOKEN",accesstoken));
        OapiCallBackDeleteCallBackRequest request = new OapiCallBackDeleteCallBackRequest ();
        request.setHttpMethod("GET");
        OapiCallBackDeleteCallBackResponse response = client.execute(request,accesstoken);
        return response.getErrmsg();
    }

    public  static String selectRegisterCallBack(String accesstoken) throws ApiException {
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.GET_CALL_BACK.replace("ACCESS_TOKEN",accesstoken));
        OapiCallBackGetCallBackRequest request = new OapiCallBackGetCallBackRequest();
        request.setHttpMethod("GET");
        OapiCallBackGetCallBackResponse response = client.execute(request,accesstoken);
        return response.getUrl();
    }
}
