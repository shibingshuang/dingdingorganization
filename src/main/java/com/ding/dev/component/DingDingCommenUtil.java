package com.ding.dev.component;


import com.ding.dev.service.LdapServices;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiDepartmentGetRequest;
import com.dingtalk.api.request.OapiGetJsapiTicketRequest;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiDepartmentGetResponse;
import com.dingtalk.api.response.OapiGetJsapiTicketResponse;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.taobao.api.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.ding.dev.domain.AccessToken;

/**
 * Created by Shibs on 2020/6/15.
 */
@Component
public class DingDingCommenUtil {
    Logger logger = LoggerFactory.getLogger(DingDingCommenUtil.class);

    // 获取access_token（GET）
    public AccessToken getToken(String appkey, String appsecret){
        String url= DingDingConstants.ACCESS_TOKEN_URL;
        AccessToken token=null;
        DefaultDingTalkClient client = new DefaultDingTalkClient(url.replace("key", appkey).replace("secret", appsecret));
        OapiGettokenRequest request = new OapiGettokenRequest();
        request.setAppkey(appkey);
        request.setAppsecret(appsecret);
        request.setHttpMethod("GET");
        try {
            OapiGettokenResponse response = client.execute(request);
            //判断是否为空
            if (response!=null){
                token=new AccessToken();
                //将获取的access_token放入accessToken对象中
                token.setAccess_token(response.getAccessToken());
            }else {
                token=null;
                // 获取token失败
                logger.error("获取token失败 errcode:{} errmsg:{}");
            }

        } catch (ApiException e) {
            e.printStackTrace();
        }
        return token;

    }

    //鉴权
    public String getJsApiTicket(String token){
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.GET_JSAPI_TICKET);
        OapiGetJsapiTicketRequest req = new OapiGetJsapiTicketRequest();
        req.setHttpMethod("GET");
        OapiGetJsapiTicketResponse rsp = null;
        try {
            rsp = client.execute(req, token);

        } catch (ApiException e) {
            e.printStackTrace();
        }
        return rsp.getErrmsg();
    }

    //根据userId获取用户详情
    public OapiUserGetResponse getUserInfo(String userId){
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.GET_USER_INFO);
        OapiUserGetRequest userRequest = new OapiUserGetRequest();
        userRequest.setUserid(userId);
        userRequest.setHttpMethod("GET");
        OapiUserGetResponse userResponse = null;
        try {
            userResponse = client.execute(userRequest, DingDingAccessTokenTask.token);
            logger.info("用户《"+userId+"》的详细信息如下：\n"+userResponse.getBody());
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return  userResponse;
    }

    //根据deptId获取部门详情
    public OapiDepartmentGetResponse getDeptInfo(String deptId){
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstants.GET_DEPT_INFO);
        OapiDepartmentGetRequest request = new OapiDepartmentGetRequest();
        request.setId(deptId);
        request.setHttpMethod("GET");
        OapiDepartmentGetResponse deptResponse = null;
        try {
            deptResponse = client.execute(request, DingDingAccessTokenTask.token);
            logger.info("部门《"+deptId+"》的详细信息如下：\n"+deptResponse.getBody());
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return deptResponse;

    }


}

