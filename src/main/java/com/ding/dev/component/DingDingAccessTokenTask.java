package com.ding.dev.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Shibs on 2020/6/22.
 */
@Component
public class DingDingAccessTokenTask {
    Logger logger = LoggerFactory.getLogger(DingDingAccessTokenTask.class);
    public static String token = "";
    public static String ticket_result = "";

    @Autowired
    private DingDingCommenUtil dingdingCommenUtil;

    // 第一次延迟1秒执行，当执行完后7100秒再执行
    @Scheduled(initialDelay = 1000, fixedDelay = 100*60*1000 )
    public void getWeixinAccessToken(){
        try {
            token = dingdingCommenUtil.getToken(DingDingConstants.APPKEY, DingDingConstants.APPSECRET).getAccess_token();
            logger.info("获取到的钉钉access_token为："+token);
            //ticket_result = dingdingCommenUtil.getJsApiTicket(token);
            //logger.info("获取票据结果为："+ticket_result);
        } catch (Exception e) {
            logger.error("获取钉钉access_toke出错，信息如下");
            e.printStackTrace();
            this.getWeixinAccessToken();
        }
    }

}


