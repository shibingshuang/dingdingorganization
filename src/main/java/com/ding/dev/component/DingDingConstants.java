package com.ding.dev.component;

import org.springframework.ldap.support.LdapNameBuilder;

import javax.naming.Name;

/**
 * Created by Shibs on 2020/6/15.
 */

public class DingDingConstants {
    public static final String O = "商越科技";
    //所有部门的父部门
    public static final String ParentDept = "People";
    //初始化用户所在组
    public static final Name groupDn = LdapNameBuilder.newInstance("ou=Group")
            .add("cn", "sunyur-user").build();

    public static final String GidNumber = "33518";

    public static final String CorpId = "ding5a74338c06b5a22d35c2f4657eb6378f";
    public static final String APPKEY = "dinggpnq53l7hlhmnmae";
    public static final String APPSECRET = "uMFkduInJrJc0dPMT69eaSNfWUmP1MTfasgQPblDmfW56WYDloGFIFCtr5qx7cG4";

    /*public static final String CorpId = "ding4ef73885ea325f6e35c2f4657eb6378f";
    public static final String APPKEY = "dingoqptmrlvshdjhlzb";
    public static final String APPSECRET = "wsiarlGMUNIK7vfl-nI6cvUP0RVeBaQCBX0JE685zSXqNdpCi5isLCVj0g-Kmkxv";*/

    /**钉钉开放平台上，开发者设置的token */
    public static final String TOKEN = "token";
    /**数据加密密钥。用于回调数据的加密，长度固定为43个字符，从a-z, A-Z, 0-9共62个字符中选取,您可以随机生成，ISV(服务提供商)推荐使用注册套件时填写的EncodingAESKey */
    public static final String AES_KEY = "abcxx7p5qnb6zs3xxxxxlkfmxqfkv23d40yd0xxxxxx";

    /**通讯录用户增加 */
    public static final String USER_ADD_ORG = "user_add_org";
    /**通讯录用户更改*/
    public static final String USER_MODIFY_ORG = "user_modify_org";
    /** 通讯录用户离职 */
    public static final String USER_LEAVE_ORG = "user_leave_org";
    /** 通讯录用户离职 */
    public static final String USER_ACTIVE_ORG = "user_active_org";
    /** 通讯录用户被设为管理员 */
    public static final String ORG_ADMIN_ADD = "org_admin_add";
    /** 通讯录用户被取消设置管理员 */
    public static final String ORG_ADMIN_REMOVE = "org_admin_remove";
    /**通讯录企业部门创建*/
    public static final String ORG_DEPT_CREATE = "org_dept_create";
    /** 通讯录企业部门修改 */
    public static final String ORG_DEPT_MODIFY = "org_dept_modify";
    /**通讯录企业部门删除*/
    public static final String ORG_DEPT_REMOVE = "org_dept_remove";
    /**企业被解散*/
    public static final String ORG_REMOVE = "org_remove";
    /**企业信息发生变更*/
    public static final String ORG_CHANGE = "org_change";
    /**员工角色信息发生变更*/
    public static final String LABEL_USER_CHANGE = "label_user_change";
    /**增加角色或者角色组*/
    public static final String LABEL_CONF_ADD = "label_conf_add";
    /**删除角色或者角色组*/
    public static final String LABEL_CONF_DEL = "label_conf_del";
    /**修改角色或者角色组*/
    public static final String LABEL_CONF_MODIFY = "label_conf_modify";
    /**测试回调接口事件类型*/
    public static final String CHECK_URL = "check_url";

    /** 获取access_token（请求方式：get） */
    public static final String ACCESS_TOKEN_URL = "https://oapi.dingtalk.com/gettoken?appkey=key&appsecret=secret";

    /** 注册事件回调接口（请求方式：post） */
    public static final String REGISTER_CALL_BACK = "https://oapi.dingtalk.com/call_back/register_call_back";

    /** 查询事件回调接口（请求方式：get） */
    public static final String GET_CALL_BACK = "https://oapi.dingtalk.com/call_back/get_call_back?access_token=ACCESS_TOKEN";

    /** 更新事件回调接口（请求方式：post） */
    public static final String UPDATE_CALL_BACK = "https://oapi.dingtalk.com/call_back/update_call_back?access_token=ACCESS_TOKEN";

    /** 删除事件回调接口（请求方式：get） */
    public static final String DELETE_CALL_BACK = "https://oapi.dingtalk.com/call_back/delete_call_back?access_token=ACCESS_TOKEN";

    /** 获取回调失败的结果 （请求方式：get）*/
    public static final String GET_CALL_BACK_FAILED_RESULT = "https://oapi.dingtalk.com/call_back/get_call_back_failed_result?access_token=ACCESS_TOKEN";

    /**获取票据*/
    public static final String GET_JSAPI_TICKET = "https://oapi.dingtalk.com/get_jsapi_ticket";

    /**获取用户详情*/
    public static final String GET_USER_INFO = "https://oapi.dingtalk.com/user/get";

    /**获取用户id*/
    public static final String GET_USER_ID = "https://oapi.dingtalk.com/user/getuserinfo";

    /**获取部门详情*/
    public static final String GET_DEPT_INFO = "https://oapi.dingtalk.com/department/get";

}

