package com.ding.dev.domain;

/**
 * Created by Shibs on 2020/6/25.
 */
public class ResponseInfo {

    private boolean success;

    private Object data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
