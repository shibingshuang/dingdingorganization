package com.ding.dev.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import javax.naming.Name;

import java.io.Serializable;

import static com.taobao.api.internal.tmc.MessageKind.Data;

/**
 * 用户类
 */
@Data
@Entry(objectClasses = {"posixAccount","inetOrgPerson","shadowAccount","top"})

public class Person {

    /**
     * 唯一标识名
     */
    @Id
    @JsonIgnore
    private Name dn;

    /**
     * 常用名称
     */
    @Attribute(name="cn")
    private String cn;

    /**
     * 用户标识
     */
    @Attribute(name="uid")
    private String uid;

    /**
     * 用户标识
     */
    @Attribute(name="uidNumber")
    private String uidNumber;

    /**
     * 用户标识
     */
    @Attribute(name="gidNumber")
    private String gidNumber;

    /**
     *
     */
    @Attribute(name="homeDirectory")
    private String homeDirectory;

    /**
     * 邮箱
     */
    @Attribute(name="mail")
    private String mail;

    /**
     *真实名称
     */
    @Attribute(name="givenName")
    private String givenName;

    /**
     *真实名称
     */
    @Attribute(name="displayName")
    private String displayName;


    /**
     *真实名称
     */
    @Attribute(name="sn")
    private String sn;


    /**
     * 密码
     */
    @Attribute(name="userPassword")
    private String userPassword;

    /**
     * 组织单元
     */
    @DnAttribute("ou")
    private String ou;

    /**
     * 企业组织
     */
    @DnAttribute("o")
    private String o;

    /**
     * 手机号
     */
    @Attribute(name = "mobile")
    private String mobile;

    /**
     * 描述：存放钉钉userId
     */
    @Attribute(name = "description")
    private String description;

    /**
     * 描述：存放dn
     */
    @Attribute(name = "destinationIndicator")
    private String destinationIndicator;

    /**
     *
     */
    @Attribute(name = "shadowFlag")
    private String shadowFlag;

    @Attribute(name = "shadowMin")
    private String shadowMin;

    @Attribute(name = "shadowMax")
    private String shadowMax;

    @Attribute(name = "shadowWarning")
    private String shadowWarning;

    @Attribute(name = "shadowInactive")
    private String shadowInactive;

    @Attribute(name = "shadowLastChange")
    private String shadowLastChange;

    @Attribute(name = "shadowExpire")
    private String shadowExpire;



    @Override
    public String toString() {
        return "Person{" +
                "dn=" + dn.toString() +
                ", cn='" + cn + '\'' +
                ", sn='" + sn + '\'' +
                ", userPassword='" + userPassword + '\'' +
                '}';
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }
    public Name getDn() {
        return dn;
    }

    public void setDn(Name dn) {
        this.dn = dn;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUidNumber() {
        return uidNumber;
    }

    public void setUidNumber(String uidNumber) {
        this.uidNumber = uidNumber;
    }

    public String getGidNumber() {
        return gidNumber;
    }

    public void setGidNumber(String gidNumber) {
        this.gidNumber = gidNumber;
    }

    public String getHomeDirectory() {
        return homeDirectory;
    }

    public void setHomeDirectory(String homeDirectory) {
        this.homeDirectory = homeDirectory;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou;
    }

    public String getO() {
        return o;
    }

    public void setO(String o) {
        this.o = o;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setGivenName(String givenName){
        this.givenName = givenName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setDisplayName(String displayName){
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getShadowFlag() {
        return shadowFlag;
    }

    public void setShadowFlag(String shadowFlag) {
        this.shadowFlag = shadowFlag;
    }

    public String getShadowMin() {
        return shadowMin;
    }

    public void setShadowMin(String shadowMin) {
        this.shadowMin = shadowMin;
    }

    public String getShadowMax() {
        return shadowMax;
    }

    public void setShadowMax(String shadowMax) {
        this.shadowMax = shadowMax;
    }

    public String getShadowWarning() {
        return shadowWarning;
    }

    public void setShadowWarning(String shadowWarning) {
        this.shadowWarning = shadowWarning;
    }

    public String getShadowInactive() {
        return shadowInactive;
    }

    public void setShadowInactive(String shadowInactive) {
        this.shadowInactive = shadowInactive;
    }

    public String getShadowLastChange() {
        return shadowLastChange;
    }

    public void setShadowLastChange(String shadowLastChange) {
        this.shadowLastChange = shadowLastChange;
    }

    public String getShadowExpire() {
        return shadowExpire;
    }

    public void setShadowExpire(String shadowExpire) {
        this.shadowExpire = shadowExpire;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDestinationIndicator() {
        return destinationIndicator;
    }

    public void setDestinationIndicator(String destinationIndicator) {
        this.destinationIndicator = destinationIndicator;
    }
}
