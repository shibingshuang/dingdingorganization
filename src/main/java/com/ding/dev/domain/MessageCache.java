package com.ding.dev.domain;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/*
 * create by shibs on 2020/8/3 0003
 */
@Configuration
public class MessageCache {
    @Bean
    public Cache<String, Integer> getCache() {
        return CacheBuilder.newBuilder().expireAfterWrite(1L, TimeUnit.SECONDS).build();// 缓存有效期为1秒
    }
}