package com.ding.dev.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import javax.naming.Name;
/*
 * create by shibs on 2020/7/10 0010
 * 组织类
 */


@Data
@Entry(objectClasses = {"organizationalUnit","top"})
public class Unit {

    /**
     * 组织
     */
    @Id
    @JsonIgnore
    private Name dn;

    /**
     * 描述
     */
    @DnAttribute("ou")
    private String ou;

    /**
     * 描述：存放钉钉部门Id
     */
    @DnAttribute("description")
    private String description;

    /**
     * 描述：存放dn
     */
    @Attribute(name = "destinationIndicator")
    private String destinationIndicator;

    @Override
    public String toString() {
        return "Unit{" +
                "dn=" + dn.toString() +
                ", ou='" + ou + '\'' +
                ", description='" + description + '\'' +
                '}';
    }


    public void setDn(Name dn) {
        this.dn = dn;
    }

    public Name getDn() {
        return dn;
    }


    public void setOu(String ou) {
        this.ou = ou;
    }

    public String getOu() {
        return ou;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDestinationIndicator() {
        return destinationIndicator;
    }

    public void setDestinationIndicator(String destinationIndicator) {
        this.destinationIndicator = destinationIndicator;
    }
}